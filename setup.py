from setuptools import setup, find_packages

setup(name='pipotron',
      install_requires=['flask','numpy'],
      version='0.0.1',
      packages=find_packages(),
      include_package_data=True,
      author='Miranda Coninx',
      author_email='Miranda@coninx.org'
)
