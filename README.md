# Pipotron

This is a simple random sentence generator that uses context-free grammars to generate bullshit.

Right now, two sorts of bullshit can be generated:
* Fake masonic titles 
* Fake Panini French comics translations (see <https://umac2.blogspot.com/2020/04/le-desastre-panini_20.html>)

## How to use

### In Python code

Import the `pipotron` module, it will expose the `Pipotron` class.

You can instantiate the class with a config file name depending on the sort of bullshit you want to generate : for example `p = Pipotron("config_maconnique.json")`

Then you can generate :
* One piece of bullshit with `p.get_phrase()`
* A list of `n` pieces of bullshit with `p.get_phrases(n)`

### As a Flask web app

 Importing `pipotron` also exposes the `create_app` function, which is a factory method returning a Flask application. It takes a config file name in argument. For example, you could use the following WSGI file with Apache mod_wsgi:

```python
from pipotron import create_app
application = create_app(config="config_maconnique.json")
```

## How to create your own bullshit

In order to create a new sort of bullshit, you must write :
* Some lists of vocabulary (terminals); those are simple text files with a terminal per line. The terminals will be drawn randomly from the lists.
* A grammar, defining how to assemble terminals to create pieces of bullshit. The grammar *must* define the non-terminal ROOT, which corresponds to one piece of bullshit. It *can* define other non-terminals. Each non-terminal is defined by a list of generation rules and an associated list of probabilities defining a discrete distribution over rules.
* (optional) A HTML template file defining how to display the bullshit in the web app, in the `templates` directory. It is optional (if omitted, `index.html` will be used) and only relevant when using the Flask web app
* A `config_<name>.json` file containing the paths of the grammar, the vocabulary files for each terminal type and, if any, the HTML template file to use.

The existing data can serve as example and documentation.

