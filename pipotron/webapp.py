#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Alex Coninx
    alex@coninx.org
    19/03/2020
""" 

from flask import Flask, render_template
from pipotron import Pipotron


def create_app(config=None):
    if not config:
        p = Pipotron()
    else:
        p = Pipotron(config)

    app = Flask(__name__)

    @app.route('/')
    def hello_world():
        phrase = p.get_phrase()
        return render_template(p.index_page, phrase=phrase)
    return app
