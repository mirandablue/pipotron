#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Alex Coninx
    19/09/2019
""" 

import os
def readwords(fname):
    out = list()
    with open(fname, 'r', encoding="utf-8") as fd:
        for l in fd:
            out.append(l.rstrip())
    return out

def reltoabspath(relpath):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)),relpath)



